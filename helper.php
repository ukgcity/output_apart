<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_SITE.'/components/com_content/helpers/route.php';

JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');


/**
 * Helper for mod_articles_news
 *
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 */


abstract class ModOutputApartHelper
{
        public static function getDB() {
	    $option = array();
            $option['driver']   = 'mysqli';
            $option['host']     = 'localhost';
            $option['user']     = 'root';
            $option['password'] = '';
            $option['database'] = 'cornel77_joom1';
            $option['prefix']   = '';

            $db = JDatabaseDriver::getInstance($option);
            	    
	    return $db;
	   
	}

        public static function getParams() {

                $app = JFactory::getApplication();
                $menu = $app->getMenu();

                $module = JModuleHelper::getModule('output_apart');
                $moduleParams = new JRegistry();
                $moduleParams->loadString($module->params);

                $mod_params = array(
                 'current_page_alias' => $menu->getActive()->alias,
                 'verkoop_alias' => $moduleParams->get('verkoop_alias'),
                 'verhuur_alias' => $moduleParams->get('verhuur_alias'),
                 'realisaties_alias' => $moduleParams->get('realisaties_alias'),
                 'buitenland_alias' => $moduleParams->get('buitenland_alias'),
                 'details_alias' => $moduleParams->get('details_alias'),
                 'verkoop_transaction_id' => $moduleParams->get('verkoop_transaction_id'),
                 'verhuur_transaction_id' => $moduleParams->get('verhuur_transaction_id'),
                 'realisaties_transaction_id' => $moduleParams->get('realisaties_transaction_id'),
                 'buitenland_transaction_id' => $moduleParams->get('buitenland_transaction_id'),
                 'fort_images_path' => $moduleParams->get('fort_images_path'),
                 'oa_thumb_width' => $moduleParams->get('oa_thumb_width'),
                 'oa_thumb_height' => $moduleParams->get('oa_thumb_height'),
                 'oa_thumb_x' => $moduleParams->get('oa_thumb_x'),
                 'oa_thumb_y' => $moduleParams->get('oa_thumb_y'),
                 'oa_dp_social_module' => $moduleParams->get('oa_dp_social_module'),
                 'cf_contact_form_successful_message' => $moduleParams->get('cf_contact_form_successful_message'),
                 'home_clickable_image' => $moduleParams->get('home_clickable_image'),
                 'verkoop_clickable_image' => $moduleParams->get('verkoop_clickable_image'),
                 'verhuur_clickable_image' => $moduleParams->get('verhuur_clickable_image'),
                 'realisaties_clickable_image' => $moduleParams->get('realisaties_clickable_image'),
                 'buitenland_clickable_image' => $moduleParams->get('buitenland_clickable_image'),
                 'home_price_visible' => $moduleParams->get('home_price_visible'),
                 'verkoop_price_visible' => $moduleParams->get('verkoop_price_visible'),
                 'verhuur_price_visible' => $moduleParams->get('verhuur_price_visible'),
                 'realisaties_price_visible' => $moduleParams->get('realisaties_price_visible'),
                 'buitenland_price_visible' => $moduleParams->get('buitenland_price_visible'),
                 'items_count_per_page' => $moduleParams->get('items_count_per_page'),
                 'new_property_day_interval' => $moduleParams->get('new_property_day_interval'),
                 'home_new_badge_visible' => $moduleParams->get('home_new_badge_visible'),
                 'verkoop_new_badge_visible' => $moduleParams->get('verkoop_new_badge_visible'),
                 'verhuur_new_badge_visible' => $moduleParams->get('verhuur_new_badge_visible'),
                 'realisaties_new_badge_visible' => $moduleParams->get('realisaties_new_badge_visible'),
                 'buitenland_new_badge_visible' => $moduleParams->get('buitenland_new_badge_visible'),
                 'frt_media_token' => $moduleParams->get('frt_media_token'),
                 'recepient_email' => $moduleParams->get('recepient_email')
                );
                if($mod_params['current_page_alias'] != $mod_params['details_alias']) {
                  $mod_params['is_current_page_clickable'] = $mod_params[$mod_params['current_page_alias'].'_clickable_image'];
                  $mod_params['is_current_page_price_visible'] = $mod_params[$mod_params['current_page_alias'].'_price_visible'];
                  $mod_params['is_current_page_new_badge_visible'] = $mod_params[$mod_params['current_page_alias'].'_new_badge_visible'];
                }
                $mp = (object)$mod_params;


                return $mp; 
        }

        public static function getAjax() {
	        // Get module parameters
	        jimport('joomla.application.module.helper');
	        $input = JFactory::getApplication()->input;
	    

	        return $input;
	}


        public static function getPropertyListAjax() {
	        jimport('joomla.application.module.helper');
	        $input = JFactory::getApplication()->input;
                $data = $input->get('data', array(), 'array');

                $mp = ModOutputApartHelper::getParams();

		$html = '';

                $zoeken_type = $data['zoeken_type'];
                $zoeken_city = $data['zoeken_city'];
                $zoeken_price_class = $data['zoeken_price_class'];
                $zoeken_sort = $data['zoeken_sort'];
                $current_page = $data['current_page'];
                $init_el_id = $data['init_el_id'];

                $where_clause = '';
                $order_clause = '';
                $sort_clause = 'empty';
                if($zoeken_type != 0) {
                  $where_clause .= ' and pm.type='.$zoeken_type;
                }
                if($zoeken_city != 0) {
                  $where_clause .= ' and pm.city="'.$zoeken_city.'"';
                }
                if($zoeken_price_class != 0) {
                  $where_clause .=  ' and pm.price_class="'.$zoeken_price_class.'"';
                }

                if(strlen(ModOutputApartHelper::getTransactionForCurrentPage($mp)) > 0) {
                  $where_clause .= ' and transaction in ('.ModOutputApartHelper::getTransactionForCurrentPage($mp).')';
                }

                if($mp->current_page_alias == $mp->buitenland_alias) {
                  $where_clause .= ' and country != 23';

                  $zoeken_land = $data['zoeken_land'];
                  if($zoeken_land != 0) {
                    $where_clause .= ' and country = '.$zoeken_land;
                  }
                } else {
                  $where_clause .= ' and country = 23';
                }

                switch($zoeken_sort) {
                  case 1:
                    $sort_clause = 'cityvalue ASC';
                    break;
                  case 2:
                    $sort_clause = 'cityvalue DESC';
                    break;
                  case 3:
                    $sort_clause = 'price ASC';
                    break;
                  case 4:
                    $sort_clause = 'price DESC';
                    break;
                  case 5:
                    $sort_clause = 'typevalue ASC';
                    break;
                  case 6:
                    $sort_clause = 'typevalue DESC';
                    break;
                }

                if($mp->current_page_alias == 'home') {
                  $order_clause = 'clicks DESC';
                }else if($mp->current_page_alias == $mp->realisaties_alias) {
                  if($sort_clause=='empty') {
                    $order_clause = 'price DESC';//, date_sold
                  } else {
                    $order_clause = $sort_clause;
                  }
                } else {
                  if($sort_clause=='empty') {
                    $order_clause = 'price ASC';
                  } else {
                    $order_clause = $sort_clause;
                  }
                }

                /* PAGINATION */
                if($current_page == -1 || $current_page == '') {
                  $start_row = 0;
                } else {
                  $start_row = ($current_page - 1)*$mp->items_count_per_page;
                }
                  $html .= '<div id="pagination_ajax">';
                  $html .= ModOutputApartHelper::getPagination($where_clause,$mp,$current_page,$init_el_id);
                  $html .= '</div>';

                /* PAGINATION */

	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
		$query->select('pm.propertyid,pm.headtypevalue,pm.currency,pm.title,pm.typevalue,pm.cityvalue,pm.price,if(date_sub(now(), interval '.$mp->new_property_day_interval.' day) <= pm.date_created,1,0) as is_new');
		$query->from('tblproperty_management pm');
                $query->where('1=1'.$where_clause);
                $query->order($order_clause);
		$db->setQuery($query,$start_row,$mp->items_count_per_page);
		$results = $db->loadObjectList();

		foreach($results as $item){
                    $item->picture_file = ModOutputApartHelper::getImageByPropertyID($item->propertyid);
                }



	        foreach ($results as $item) {
                  $html .= '<div class="product">';
                  $html .= ' <div class="product-image">';
                  $html .= '  <ul id="da-thumbs" class="da-thumbs">';
                  $html .= '    <li>';
                  $html .= '     <a ';
                  if($mp->is_current_page_clickable==1) {
                    $html .= 'href="'.JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid.'" target="_self"';
                  }
                  $html .= '>';
                  $html .= '       <img src="'.JURI::root().'/'.$mp->fort_images_path.$item->picture_file.'" alt="Image one">';
                  $html .= '       <div class="da-thumbs-div" style="display: none; left: -100%; top: 0px; transition: all 300ms ease; -webkit-transition: all 300ms ease;"><span><img src="'.JURI::root().'modules/mod_output_apart/assets/images/zoom-white.png"></span></div>';
                  if($mp->is_current_page_new_badge_visible == 1 && $item->is_new == 1) {
                    $html .= '     <div class="new_badge">';
                    $html .= '         <span class="new_badge_title">NIEUW</span>';
                    $html .= '     </div>';
                  }
                  $html .= '     </a>';
                  $html .= '    </li>';
                  $html .= '  </ul>';
                  $html .= ' </div><!--product image-->';
                  $html .= ' <h4><a ';
                  if($mp->is_current_page_clickable==1) {
                    $html .= 'href="'.JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid.'" target="_self"';
                  }
                  $html .= '>'.$item->title.'</a></h4>';
                  $html .= ' <h6>'.$item->typevalue.' - '.$item->cityvalue.'</h6>';
                  $html .= ' <div class="price-area">';
                  $html .= '  <div class="amount">';
                  $html .= '   <a ';
                  if($mp->is_current_page_clickable==1) {
                    $html .= 'href="'.JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid.'" target="_self"';
                  }
                  $html .= '>';
                  if($mp->is_current_page_price_visible==1) {
                    $html .= $item->currency.' '.number_format($item->price,2,",",".");
                  }
                  $html .= '</a>';
                  $html .= '  </div><!--amount-->';
                  $html .= '  <div class="zoom-icon">';
                  if($mp->is_current_page_clickable==1) {
                    $html .= '<a href="'.JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid.'" target="_self"><img src="'.JURI::root().'modules/mod_output_apart/assets/images/zoom-black.png" alt="Zoom Black"></a>';
                  }
                  $html .= '  </div><!--zoom icon-->';
                  $html .= '  <div class="clear"></div>';
                  $html .= ' </div><!--price area-->';
                  $html .= '</div><!--product-->';

	        }


	        return $html;
	   
	}
	public static function getPagination($where_clause,$mp,$current_page,$init_el_id) {

	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
                $query->select('COUNT(pm.propertyid) rows_count');
		$query->from('tblproperty_management pm');
                $query->where('1=1 '.$where_clause);
		$db->setQuery($query);
                $results = $db->loadObject();

                $pc = ceil($results->rows_count / $mp->items_count_per_page);
                $html = '';

                if($pc > 1) {
                  $i = 2;
                  if($init_el_id == 'pagination') {
                    $i = 1;
                  } else {
                    $i = 2;
                    $html .= '<a class="current">1</a>';
                  }
                  for($i; $i <= $pc; $i++) {
                    $html .= '<a ';
                    if($init_el_id == 'pagination' && $i == $current_page) {
                      $html .= 'class="current" ';
                    } else {
                      $html .= 'class="nc" href="#"';
                    }
                    $html .= '>'.$i.'</a>';
                  }
                  //$html .= '<a href="#" class="nc next">&gt;&gt;</a>';
                }


                return $html;
        }

        public static function setQuestionAjax() {
	        jimport('joomla.application.module.helper');
	        $input = JFactory::getApplication()->input;
                $data = $input->get('data', array(), 'array');

                $mp = ModOutputApartHelper::getParams();


                $cf_name = $data['cf_name'];
                $cf_surname = $data['cf_surname'];
                $cf_phone = $data['cf_phone'];
                $cf_email = $data['cf_email'];
                $cf_question = $data['cf_question'];
                $cf_property_id = $data['cf_property_id'];
                $cf_country = $data['cf_country'];

                $PropertyDetails = ModOutputApartHelper::getDetailsInfoByPropertyID($cf_property_id);
                $page_alias = ModOutputApartHelper::getPageAliasByTransactionId($mp, $PropertyDetails->transaction);


	        $db = ModOutputApartHelper::getDB();
                $columns = array('media_token', 'country', 'date_created', 'firstname', 'lastname', 'lang', 'phone', 'email', 'question', 'propertyid');
                $values = array($db->quote($mp->frt_media_token), $db->quote($cf_country), 'now()', $db->quote($cf_name), $db->quote($cf_surname), $db->quote('NL'), $db->quote($cf_phone), $db->quote($cf_email), $db->quote($cf_question), $cf_property_id);
		$query = $db->getQuery(true);
		$query
                     ->insert($db->quoteName('tblexchange_contact_management'))
                     ->columns($db->quoteName($columns))
                     ->values(implode(',', $values));
		$db->setQuery($query);
		$results = $db->query();

		if($results==1) {
                  $html = str_replace("[firstname]",$cf_name, $mp->cf_contact_form_successful_message);
                  $html = str_replace("[lastname]",$cf_surname, $html);
                  $html = '<div class="form-title">'.$html.'</div>';
                } else {
                  $html = 'Error. Try again.';
                }
				
                /* SENDING AN EMAIL */
                $mailer = JFactory::getMailer();
                $config = JFactory::getConfig();
                $sender = array( 
                   $cf_email,
                   $cf_name.' '.$cf_surname
                );
                $mailer->setSender($sender);

                $mailer->addRecipient($mp->recepient_email);
                $body = '<h2>Aanvraag pand: '.$PropertyDetails->title.'</h2>';
                $body .= '<div><strong>Woning ID:</strong> <a href="'.JURI::root().'/'.$mp->details_alias.'?pagetype=details&propid='.$PropertyDetails->propertyid.'" target="_blank">'.$cf_property_id.'</a></div>';
                $body .= '<div><strong>Voornaam:</strong> '.$cf_name.'</div>';
                $body .= '<div><strong>Achternaam:</strong> '.$cf_surname.'</div>';
                $body .= '<div><strong>Telefoon:</strong> '.$cf_phone.'</div>';
                $body .= '<div><strong>E-mail:</strong> '.$cf_email.'</div>';
                $body .= '<div><strong>Vraag:</strong> '.$cf_question.'</div>';
                $mailer->isHTML(true);
                $mailer->Encoding = 'base64';
                $mailer->setSubject('Aanvraag pand: '.$PropertyDetails->title);
                $mailer->setBody($body);

                $send = $mailer->Send();
                if ($send !== true) {
                   $html .= 'Error sending email: ' . $send->__toString();
                }
                
	        return $html;
	   
	}

	public static function getTypeList(&$mp) {
	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
		$query->select('id as id, value as type_name');
		$query->from('tblproperty_type');
                $query->order('2 ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();


		return $results;
        }

	public static function getLandList(&$mp) {
	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
		$query->select('DISTINCT country, countryvalue');
		$query->from('tblproperty_management');
                $where_clause = '';
                if($mp->current_page_alias == $mp->buitenland_alias) {
                  $where_clause .= ' and country != 23';
                } else {
                  $where_clause .= ' and country = 23';
                }
                $query->where('1=1 '.$where_clause);
                $query->order('2 ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();


		return $results;
        }

	public static function getCityList(&$mp) {
	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
		$query->select('DISTINCT city as city, cityvalue as city_name, country');
		$query->from('tblproperty_management');
                $where_clause = '';
                if($mp->current_page_alias == $mp->buitenland_alias) {
                  $where_clause .= ' and country != 23';
                } else {
                  $where_clause .= ' and country = 23';
                }
                $query->where('1=1 '.$where_clause);
                $query->order('2 ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();


		return $results;
        }


	public static function getPriceClassList(&$mp) {
	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
		$query->select('DISTINCT price_class as price_class');
		$query->from('tblproperty_management');
                $where_clause = '';
                if(strlen(ModOutputApartHelper::getTransactionForCurrentPage($mp)) > 0) {
                  $where_clause .= ' and transaction in ('.ModOutputApartHelper::getTransactionForCurrentPage($mp).')';
                }
                if($mp->current_page_alias == $mp->buitenland_alias) {
                  $where_clause .= ' and country != 23';
                } else {
                  $where_clause .= ' and country = 23';
                }
                $query->where('1=1 '.$where_clause);
                $query->order('convert(trim(substr(price_class,instr(price_class,"-")+2,10)),unsigned) ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();


		return $results;
        }

	public static function getPropertyList($mp,$limit,$mode='normal') {
	        $db = ModOutputApartHelper::getDB();
		$query = $db->getQuery(true);
                if($mode=='pagination') {
		  $query->select('COUNT(pm.propertyid) rows_count');
                } else {
		  $query->select('pm.propertyid,pm.headtypevalue,pm.currency,pm.title,pm.typevalue,pm.cityvalue,pm.price,pm.transaction,if(date_sub(now(), interval '.$mp->new_property_day_interval.' day) <= pm.date_created,1,0) as is_new');
                }
		$query->from('tblproperty_management pm,tblproperty_extra_field pef');
                $where_clause = '';
                $order_clause = '';

                if(strlen(ModOutputApartHelper::getTransactionForCurrentPage($mp)) > 0) {
                  $where_clause .= ' and pm.transaction in ('.ModOutputApartHelper::getTransactionForCurrentPage($mp).')';
                }
                if($mp->current_page_alias == $mp->buitenland_alias) {
                  $where_clause .= ' and pm.country != 23';
                } else {
                  $where_clause .= ' and pm.country = 23';
                }
                if($mp->current_page_alias == 'home') {
                  $order_clause = 'pm.propertyid ASC';
                  $where_clause .= ' and upper(pef.extra_field_value) = upper("True")';
                }else if($mp->current_page_alias == $mp->realisaties_alias) {
                  $order_clause = 'pm.price DESC';//, date_sold
                } else {
                  $order_clause = 'pm.price ASC';
                }
                $query->where('1=1 and pm.propertyid = pef.propertyid and pef.extra_field_id = 10687 '.$where_clause);
                $query->order($order_clause);
		$db->setQuery($query,0,$limit);
                if($mode=='pagination') {
		  $results = $db->loadObject();
                  return $results->rows_count;
                } else {
                  $results = $db->loadObjectList();
                }

                foreach($results as $item){
                    $item->picture_file = ModOutputApartHelper::getImageByPropertyID($item->propertyid);
                    //$item->page_alias = ModOutputApartHelper::getPageAliasByTransactionId($mp, $item->transaction);
                    $item->page_alias = $mp->details_alias;
                }


		return $results;
        }

	public static function getDetailsInfoByPropertyID($property_id) {
	        $db = ModOutputApartHelper::getDB();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('tblproperty_management');
                $query->where('propertyid = '.$property_id);
		$db->setQuery($query);
		$results = $db->loadObject();

		return $results;
        }

	public static function getImageByPropertyID($property_id) {
	        $db = ModOutputApartHelper::getDB();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('tblproperty_picture');
                $query->where('propertyid = '.$property_id);
                $query->order('picture_index ASC');
		$db->setQuery($query);
		$results = $db->loadObject();

		return $results->picture_file;
        }

	public static function getLayoutByPropertyID($property_id) {
	        $db = ModOutputApartHelper::getDB();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('tblproperty_layout');
                $query->where('propertyid = '.$property_id);
                $query->order('id ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();

		return $results;
        }

	public static function getListImageByPropertyID($property_id) {
	        $db = ModOutputApartHelper::getDB();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('tblproperty_picture');
                $query->where('propertyid = '.$property_id);
                $query->order('picture_index ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();

		return $results;
        }

	public static function getTransactionForCurrentPage($mp) {
                $transaction_id = "";

                switch($mp->current_page_alias) {
                  case $mp->verkoop_alias:
                      $transaction_id = $mp->verkoop_transaction_id;
                      break;
                  case $mp->verhuur_alias:
                      $transaction_id = $mp->verhuur_transaction_id;
                      break;
                  case $mp->realisaties_alias:
                      $transaction_id = $mp->realisaties_transaction_id;
                      break;
                  case $mp->buitenland_alias:
                      $transaction_id = $mp->buitenland_transaction_id;
                      break;
                }

		return $transaction_id;
        }

	public static function getPageAliasByTransactionId($mp, $transaction_id) {
                $page_alias = "";

                switch($transaction_id) {
                  case $mp->verkoop_transaction_id:
                      $page_alias = $mp->verkoop_alias;
                      break;
                  case $mp->verhuur_transaction_id:
                      $page_alias = $mp->verhuur_alias;
                      break;
                  case $mp->realisaties_transaction_id:
                      $page_alias = $mp->realisaties_alias;
                      break;
                  case $mp->buitenland_transaction_id:
                      $page_alias = $mp->buitenland_alias;
                      break;
                }

		return $page_alias;
        }

}
