<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
	
	jQuery(document).ready(function(){
	  gmap_initialize();
	});

      function gmap_initialize() {
        var myLatlng = new google.maps.LatLng(<?php echo $DetailsInfo->latitude.",".$DetailsInfo->longitude;?>);
        var mapOptions = {
            zoom: 14,
            center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('map-canv'), mapOptions);
        
	var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: ''
        });
      }
    </script>

<div id="page-preloader" style="display:none;"><span class="spinner"></span></div>
<div id="wrapper-page">
	<div class="wrapper">
    	<div id="single-house-area">
        	<div class="house-information">
            	<div class="house-images-area">
                	<div class="house-big-image">
					   <a href="<?php echo JURI::root().$mp->fort_images_path.$big_image_path;?>" rel="prettyPhoto">
                    	<img src="<?php echo JURI::root().$mp->fort_images_path.$big_image_path;?>" alt="">
					   </a>
                    </div><!--house big image-->
                    <div class="house-small-images">
                    <?php 
                      foreach ($DetailsImageList as $item) {
                        echo '<a href="'.JURI::root().$mp->fort_images_path.$item->picture_file.'" rel="prettyPhoto[pp_gal]">';
                        echo '<img src="'.JURI::root().$mp->fort_images_path.$item->picture_thumb.'" alt="">';
			echo '</a>';
                      }
                    ?>                    	
                        <div class="clear"></div>
                    </div><!--house small images-->
                </div><!--house imagea area-->
                <div class="property-info">
                	<h2><?php echo $DetailsInfo->title;?></h2>
                    <h3>€ <?php echo number_format($DetailsInfo->price,2,",",".");?></h3>
                    <div class="black-line"></div>
                    <div class="property-content p-top">
                        <h5>Beschrijving</h5>
                        <p><?php echo $DetailsInfo->description;?></p>
                        <ul class="listing-property">
                            <li><span class="bold-text">Type:</span> <?php echo $DetailsInfo->typevalue;?></li>
                            <li><span class="bold-text">Referentie:</span> <?php echo $DetailsInfo->reference;?></li>
                            <?php if($DetailsInfo->area_ground != "0") : ?>
                            <li><span class="bold-text">Totale oppervlakte:</span> <?php echo $DetailsInfo->area_ground;?>m²</li>
                            <?php endif; 
                                  if($DetailsInfo->non_indexed_ki != "0") : ?>
                            <li><span class="bold-text">Niet geïndexeerd:</span> <?php echo $DetailsInfo->non_indexed_ki;?>€</li>
                            <?php endif; 
                                  if($DetailsInfo->epc_value != "0") : ?>
                            <li><span class="bold-text">EPC:</span> <?php echo $DetailsInfo->epc_value;?> kWh/m²</li>
                            <?php endif; ?>
                        </ul>
                    </div><!--property content-->
                    <div class="property-content p-bot">
                        <h5>Indeling / Technisch</h5>
                        <table width="100%" border="0">
                          <tbody><tr>
                            <th scope="col" class="table-col-one">Naam</th>
                            <th scope="col" class="table-col-two">Aantal</th>
                            <th scope="col" class="table-col-three">Opp.</th>
                            <th scope="col" class="table-col-four">Commentaar</th>
                          </tr>
                          <?php 
                          $class_counter = 0;
                          foreach ($LayoutInfo as $item) {?>
                          <tr>
                            <td <?php echo (( $class_counter%2 ) ? 'class="gray-bg"' : ''); ?>><?php echo $item->value;?></td>
                            <td <?php echo (( $class_counter%2 ) ? 'class="gray-bg"' : ''); ?>><?php echo $item->count;?></td>
                            <td <?php echo (( $class_counter%2 ) ? 'class="gray-bg"' : ''); ?>><?php echo $item->surface;?></td>
                            <td <?php echo (( $class_counter%2 ) ? 'class="gray-bg"' : ''); ?>><?php echo $item->comment;?></td>
                          </tr>
                          <?php $class_counter++; }?>
                        </tbody></table>
                	</div><!--property info-->
                    <div class="black-line"></div>
                    	<div class="socials-icons">
                          <?php
                            jimport( 'joomla.application.module.helper' );
                            $module = JModuleHelper::getModule($mp->oa_dp_social_module);
                            $attribs['style'] = 'xhtml';
                            echo JModuleHelper::renderModule( $module, $attribs );
                          ?>
                        </div><!--social icons-->
                    <div class="black-line"></div>
                </div><!--house all info-->
                <div class="clear"></div>
            </div><!--house information-->     
            <div id="contact-form-area">
            	<div class="form-title">
                	Meer info omtrent deze woning?
                </div><!--form title-->
                <p>Bedankt voor het invullen van het contactformulier. Eén van onze medewerkers contacteert u zo spoedig mogelijk. </p>
                <div class="black-line white-line"></div>
                <div id="contact-form-property">
                 <form id="details-contact-form" class="form-validate" >
                   <input id="cf_property_id" type="hidden" value="<?php echo $cf_property_id;?>">
                   <input id="cf_name" type="text" class="input-form required" size="30" placeholder="Voornaam*">
                   <input id="cf_surname" type="text" class="input-form required" placeholder="Achternaam*">
                   <input id="cf_phone" type="text" class="input-form required" placeholder="Telefoon*">
                   <input id="cf_email" type="text" class="input-form input-last required validate-email" placeholder="E-mail*">
                   <input id="cf_country" type="hidden" value="<?php echo $DetailsInfo->country;?>">
                   <div class="clear"></div>
                   <textarea id="cf_question" class="textarea-form" placeholder="Vragen of opmerkingen?"></textarea>
                   <input id="contact-form-submit" type="submit" class="submit-form-button validate" value="Bericht verzenden">
                 </form>
                </div><!--contact form property-->
            </div><!--contact-form area-->
            <div id="map-area">
	        <div id="map-canv" style="width:100%; height:350px;"></div>
                <a class="map-links">LIGGING</a>
            </div><!--map area-->
        </div><!--single house area-->
    </div><!--wrapper-->
</div><!--wrapper page-->