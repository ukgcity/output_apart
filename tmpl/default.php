<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div id="page-preloader" style="display:none;"><span class="spinner"></span></div>

<div id="wrapper-page">
    <div class="wrapper">
        <div id="product-searching-area">
            <div class="search-heading">
                Zoek pand:
            </div><!--search heading-->
            <select id="zoeken_type" class="select-option">
              <option selected="selected" value="0">- Type -</option>
	      <?php 
              foreach ($TypeList as $item) {
                echo "<option value=".$item->id.">".$item->type_name."</option>";
              }
              ?>
            </select>
            <?php if($mp->current_page_alias == $mp->buitenland_alias) :?>
            <select id="zoeken_land" class="select-option">
              <option selected="selected" value="0">- Land -</option>
	      <?php 
              foreach ($LandList as $item) {
                echo "<option value='".$item->country."'>".$item->countryvalue."</option>";
              }
              ?>
            </select>
            <?php endif;?>
            <select id="zoeken_city" class="select-option">
              <option selected="selected" value="0">- Gemeente -</option>
	      <?php 
              foreach ($CityList as $item) {
                echo "<option value='".$item->city."' atr-land='".$item->country."'>".$item->city_name."</option>";
              }
              ?>
            </select>
            <select id="zoeken_price_class" class="select-option">
             <option selected="selected" value="0">- Prijsklasse -</option>
	      <?php 
              foreach ($PriceClassList as $item) {
                echo "<option value='".$item->price_class."'>".$item->price_class."</option>";
              }
              ?>
            </select>
            <select id="zoeken_sort" class="select-option">
             <option selected="selected" value="0">- Sorteren -</option>
             <option value="1">Gemeente A-Z</option>
             <option value="2">Gemeente Z-A</option>
             <option value="3">Prijs 0-9</option>
             <option value="4">Prijs 9-0</option>
             <option value="5">Type A-Z</option>
             <option value="6">Type Z-A</option>
            </select>
            <input id="product-search-submit" type="submit" class="submit-search" value="Zoeken">
            <div class="clear"></div>
        </div><!--product searching area-->
        <div id="product-pagination" class="product-pagination pagination-top">
         <?php
          if($PagesCount != 1) :
         ?>
         <a class="current">1</a>
         <?php for($pag = 2; $pag <= $PagesCount; $pag++):?>
         <a class="nc" href="#"><?php echo $pag; ?></a>
         <?php endfor; ?>
         <!--<a href="#" class="nc next">&gt;&gt;</a>-->
         <?php endif; ?>
        </div>
        <br />
        <div id="products-area">
        
	<?php foreach ($PropertyList as $item) : ?>
        <div class="product">
            <div class="product-image">
                <ul id="da-thumbs" class="da-thumbs">
                    <li>
                        <a <?php if($mp->is_current_page_clickable==1) :?>href="<?php echo JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self" <?php endif; ?>>
                            <img src="<?php echo JURI::root().$mp->fort_images_path.$item->picture_file;?>" alt="Image one">
                            <div class="da-thumbs-div" style="display: none; left: -100%; top: 0px; transition: all 300ms ease; -webkit-transition: all 300ms ease;"><span><img src="<?php echo JURI::root().$module_path;?>/assets/images/zoom-white.png"></span></div>
                            <?php if($mp->is_current_page_new_badge_visible==1 && $item->is_new == 1) :?>
                            <div class="new_badge">
                                <span class="new_badge_title">NIEUW</span>
                            </div>
                            <?php endif;?>
                        </a>
                    </li>
                </ul>
            </div><!--product image-->
            <h4><a <?php if($mp->is_current_page_clickable==1) :?>href="<?php echo JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self" <?php endif; ?>>
                   <?php echo $item->title;?>
                </a>
            </h4>
            <h6><?php echo $item->typevalue." - ".$item->cityvalue;?></h6>
            <div class="price-area">
                <div class="amount">
                    <a <?php if($mp->is_current_page_clickable==1) :?>href="<?php echo JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self" <?php endif; ?>><?php if($mp->is_current_page_price_visible==1) { echo $item->currency.'&nbsp;'.number_format($item->price,2,",",".");} ?> </a>
                </div><!--amount-->
                <div class="zoom-icon">
                    <?php if($mp->is_current_page_clickable==1) :?>
                    <a href="<?php echo JURI::root().$mp->details_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self"><img src="<?php echo JURI::root().$module_path;?>/assets/images/zoom-black.png" alt="Zoom Black"></a>
                    <?php endif; ?>
                </div><!--zoom icon-->
                <div class="clear"></div>
            </div><!--price area-->
        </div><!--product-->
	<?php endforeach; ?>
        
            <div class="clear"></div>
        </div><!--products area-->
        <div class="clear"></div>
        <div id="product-pagination" class="product-pagination">
         <?php
          if($PagesCount != 1) :
         ?>
         <a class="current">1</a>
         <?php for($pag = 2; $pag <= $PagesCount; $pag++):?>
         <a class="nc" href="#"><?php echo $pag; ?></a>
         <?php endfor; ?>
         <!--<a href="#" class="nc next">&gt;&gt;</a>-->
         <?php endif; ?>
        </div>

    </div><!--wrapper-->
</div>

