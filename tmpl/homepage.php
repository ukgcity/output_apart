<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div id="wrapper-page">
    <div class="wrapper">

        <div id="products-area">
        
	<?php foreach ($PropertyList as $item) : ?>
        <div class="product">
            <div class="product-image">
                <ul id="da-thumbs" class="da-thumbs">
                    <li>
                        <a <?php if($mp->is_current_page_clickable==1) :?>href="<?php echo JURI::current().$item->page_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self" <?php endif; ?>>
                            <img src="<?php echo JURI::root().$mp->fort_images_path.$item->picture_file;?>" alt="Image one">
                            <div class="da-thumbs-div" style="display: none; left: -100%; top: 0px; transition: all 300ms ease; -webkit-transition: all 300ms ease;"><span><img src="<?php echo JURI::root().$module_path;?>/assets/images/zoom-white.png"></span></div>
                            <?php if($mp->is_current_page_new_badge_visible==1 && $item->is_new == 1) :?>
                            <div class="new_badge">
                                <span class="new_badge_title">NIEUW</span>
                            </div>
                            <?php endif;?>
                        </a>
                    </li>
                </ul>
            </div><!--product image-->
            <h4><a <?php if($mp->is_current_page_clickable==1) :?>href="<?php echo JURI::current().$item->page_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self" <?php endif; ?>><?php echo $item->title;?></a></h4>
            <h6><?php echo $item->typevalue." - ".$item->cityvalue?></h6>
            <div class="price-area">
                <div class="amount">
                    <a <?php if($mp->is_current_page_clickable==1) :?>href="<?php echo JURI::current().$item->page_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self" <?php endif; ?>><?php if($mp->is_current_page_price_visible==1) { echo $item->currency.'&nbsp;'.number_format($item->price,2,",",".");} ?> </a>
                </div><!--amount-->
                <div class="zoom-icon">
                    <?php if($mp->is_current_page_clickable==1) :?>
                    <a href="<?php echo JURI::current().$item->page_alias.'?pagetype=details&propid='.$item->propertyid;?>" target="_self"><img src="<?php echo JURI::root().$module_path;?>/assets/images/zoom-black.png" alt="Zoom Black"></a>
                    <?php endif; ?>
                </div><!--zoom icon-->
                <div class="clear"></div>
            </div><!--price area-->
        </div><!--product-->
	<?php endforeach; ?>
        
            <div class="clear"></div>
        </div><!--products area-->
    </div><!--wrapper-->
</div>