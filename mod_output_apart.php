<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//$loadJquery = $params->get('loadJquery', 1);
$format     = $params->get('format', 'debug');

JHtml::_('behavior.caption');

// Load jQuery
JHtml::_('jquery.framework');

JHtml::_('behavior.framework', true);

// Load validation
JHTML::_('behavior.formvalidation');

$module_path = 'modules/'.$module->module;

$document = JFactory::getDocument();
$document->addStyleSheet($module_path.'/assets/css/style.css', "text/css");
$document->addStyleSheet($module_path.'/assets/css/css', "text/css");
$document->addStyleSheet($module_path.'/assets/css/prettyPhoto.css', "text/css");
$document->addScript($module_path.'/assets/js/modernizr.custom.97074.js', "text/javascript");
//$document->addScript($module_path.'/assets/js/jquery.min.js', "text/javascript");
$document->addScript($module_path.'/assets/js/jquery.hoverdir.js', "text/javascript");
$document->addScript($module_path.'/assets/js/jquery.prettyPhoto.js', "text/javascript");
$document->addScript($module_path.'/assets/js/jqthumb.js', "text/javascript");

$app = JFactory::getApplication();
$menu = $app->getMenu();


// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

/* PARAMS */

/*$mod_params = array(
 'current_page_alias' => '',
 'verkoop_alias' => $params->get('verkoop_alias'),
 'verhuur_alias' => $params->get('verhuur_alias'),
 'realisaties_alias' => $params->get('realisaties_alias'),
 'verkoop_transaction_id' => $params->get('verkoop_transaction_id'),
 'verhuur_transaction_id' => $params->get('verhuur_transaction_id'),
 'realisaties_transaction_id' => $params->get('realisaties_transaction_id'),
);
$mp = (object)$mod_params;*/

$mp = ModOutputApartHelper::getParams();

/* PARAMS */

/* PAGINATION */
$ItemsCount = ModOutputApartHelper::getPropertyList($mp,0,'pagination');
$PagesCount = ceil($ItemsCount / $mp->items_count_per_page);
/* PAGINATION */

/* AJAX */



$js = <<<JS

jQuery(document).ready(function() {
    setImages();
    jQuery("a[rel^='prettyPhoto']").prettyPhoto();
    setPagination();

    jQuery('#zoeken_land').change(function(){
      jQuery('#zoeken_city option').show();
      if(jQuery(this).val() != 0) {
        jQuery('#zoeken_city option[atr-land!=' + jQuery(this).val() + ']').hide();
      }
      jQuery('#zoeken_city option[value=0]').show().prop('selected', true);
    });

});

jQuery(document).on('click', '#product-search-submit', function() {
    preloaderOn();
    getProperty(-1,'product-search-submit');
    return false;
});

jQuery(document).on('submit', '#details-contact-form', function() {
    preloaderOn();
    setQuestion();
    return false;
});

function preloaderOn() {
    var preloader = jQuery('#page-preloader'),
        spinner = jQuery(preloader).find('.spinner');
    jQuery(spinner).fadeIn();
    jQuery(preloader).delay(350).fadeIn('slow');
}

function preloaderOff() {
    var preloader = jQuery('#page-preloader'),
        spinner = jQuery(preloader).find('.spinner');
    jQuery(spinner).fadeOut();
    jQuery(preloader).delay(350).fadeOut('slow');
}

function setPagination(){
    jQuery('.product-pagination a').click(function(){
      if(jQuery(this).hasClass('current')) {
        return false;
      }
      preloaderOn();
      getProperty(jQuery(this).text(),'pagination');
      jQuery('.product-pagination a.current').removeClass('current').addClass('nc').attr('href','#');
      jQuery(this).removeClass('nc').addClass('current').removeAttr('href').blur();
    });
}

function setImages() {
    jQuery('#da-thumbs > li').each(function() {
        jQuery(this).hoverdir({
            hoverDelay: 75
        });
    });

    jQuery('#da-thumbs > li > a > img').each(function() {
        jQuery(this).jqthumb({
            width: '{$mp->oa_thumb_width}',
            height: '{$mp->oa_thumb_height}',
            position: {
                y: '{$mp->oa_thumb_y}',
                x: '{$mp->oa_thumb_x}'
            }
        });
    });
}

function getProperty(current_page,init_el_id){

    if ((current_page == -1) && jQuery('#zoeken_sort').val() != 0) {
      current_page = jQuery('.product-pagination .pagination-top a.current').text();
    }

    var value = {
            'zoeken_type': jQuery('#zoeken_type').val(),
            'zoeken_land': jQuery('#zoeken_land').val(),
            'zoeken_city': jQuery('#zoeken_city').val(),
            'zoeken_price_class': jQuery('#zoeken_price_class').val(),
            'zoeken_sort': jQuery('#zoeken_sort').val(),
            'current_page': current_page,
            'init_el_id': init_el_id
            },
        method = 'getPropertyList',
        data_el_id = '#products-area',
        data_el_child = 'div.product';

        sendAjax(value, method, data_el_id, data_el_child);
}

function setQuestion(){
    var value = {
            'cf_name': jQuery('#cf_name').val(),
            'cf_surname': jQuery('#cf_surname').val(),
            'cf_phone': jQuery('#cf_phone').val(),
            'cf_email': jQuery('#cf_email').val(),
            'cf_question': jQuery('#cf_question').val(),
            'cf_property_id': jQuery('#cf_property_id').val(),
            'cf_country': jQuery('#cf_country').val()
            },
        method = 'setQuestion',
        data_el_id = '#contact-form-area',
        data_el_child = 'div.form-title';

        sendAjax(value, method, data_el_id, data_el_child);
}

function sendAjax(ajax_value, ajax_method, ajax_data_el_id, ajax_data_el_child) {
    var action = jQuery(this).attr('class'),
        request = {
            'option': 'com_ajax',
            'module': 'output_apart',
            'cmd': action,
            'data': ajax_value,
            'format': '{$format}',
            'method': ajax_method
        };

    jQuery.ajax({
        type: 'POST',
        data: request,
        success: function(response) {
            jQuery(ajax_data_el_id).empty();
            jQuery(ajax_data_el_id).html(response);
            jQuery(ajax_data_el_id + ' ' + ajax_data_el_child).unwrap();

            if(ajax_method == 'getPropertyList'){
              jQuery('.product-pagination').empty();
              jQuery('.product-pagination').html(jQuery('#pagination_ajax').html());
              jQuery('#pagination_ajax').empty();
              jQuery('#pagination_ajax').remove();
              setPagination();
            }

            preloaderOff();
            setImages();
        },
        error: function(response) {
            console.log(response);
        }
    });
    return false;
}
JS;

$document->addScriptDeclaration($js);



$TypeList = ModOutputApartHelper::getTypeList($mp);
$CityList = ModOutputApartHelper::getCityList($mp);
$PriceClassList = ModOutputApartHelper::getPriceClassList($mp);
$LandList = ModOutputApartHelper::getLandList($mp);



//echo JRequest::getVar('pagetype', 'default');

//$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));



if ($menu->getActive() == $menu->getDefault()) {
   //$mp->current_page_alias = 'home';

   $PropertyList = ModOutputApartHelper::getPropertyList($mp,4);
   require JModuleHelper::getLayoutPath('mod_output_apart', $params->get('layout', 'homepage'));
} else {
   //$mp->current_page_alias = $menu->getActive()->alias;

   switch($mp->current_page_alias) {
     case $mp->details_alias:
          if(JRequest::getVar('pagetype') == 'details' && JRequest::getVar('propid')) {
            $cf_property_id = JRequest::getVar('propid');
            $big_image_path = ModOutputApartHelper::getImageByPropertyID(JRequest::getVar('propid'));
            $DetailsImageList = ModOutputApartHelper::getListImageByPropertyID(JRequest::getVar('propid'));
            $DetailsInfo = ModOutputApartHelper::getDetailsInfoByPropertyID(JRequest::getVar('propid'));
            $LayoutInfo = ModOutputApartHelper::getLayoutByPropertyID(JRequest::getVar('propid'));

            $document->setTitle($DetailsInfo->title.' - '.$document->getTitle());

            require JModuleHelper::getLayoutPath('mod_output_apart', $params->get('layout', 'details'));
        }
        break;
     case $mp->verkoop_alias:
     case $mp->verhuur_alias:
     case $mp->realisaties_alias:
     case $mp->buitenland_alias:
          $PropertyList = ModOutputApartHelper::getPropertyList($mp,$mp->items_count_per_page);
          require JModuleHelper::getLayoutPath('mod_output_apart', $params->get('layout', 'default'));
        break;
   }
}

